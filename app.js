var express = require('express'),
	exphbs  = require('express-handlebars'),
	extend  = require('jquery-extend'),
	fs 		= require('fs'),
	moment  = require('moment'),
	path    = require('path'),
	yaml    = require('yamljs');

var app = express();

app.engine('handlebars', exphbs({
	defaultLayout : 'main'
}));

app.set('view engine', 'handlebars');

// Get the role files
let roles = {};
const files = fs.readdirSync('data');
files.forEach(file => {
	const data = fs.readFileSync(`data/${file}`);
	const str = data.toString();
	const name = path.parse(file).name;
	roles[name] = yaml.parse(str);
});

// Extend the roles and populate the object
let go = true;
while (go == true) {
	go = false;
	for (const slug in roles) {
		const role = roles[slug];
		if (role.hasOwnProperty('extends')) {
			const toExtendSlug = role.extends[0];
			const toExtend = roles[toExtendSlug];
			if(toExtend.hasOwnProperty('extends') == false) {
				roles[slug] = extend(true, {}, toExtend, role);
				delete roles[slug].extends;
			} else {
				go = true;
			}
		}
	}
}
app.set('roles', roles);

app.get('/', (req, res) => {
    res.render('home', { roles });
});

app.get('/:role.tsv', (req, res) => {
	let obj = roles[req.params.role];
	obj.slug = req.params.role;
	obj.layout = 'excel';
	obj.timestamp = moment().format('MMMM Do YYYY, h:mm:ss a')
	res.set({
		'Content-Disposition' : `attachment;filename=${obj.slug}.tsv`,
		'Content-Type'        : 'text/tab-separated-values'
	});
	res.render('role-description', obj);
});

app.get('/:role', (req, res) => {
	let obj = roles[req.params.role];
	obj.slug = req.params.role;
	res.render('role-description', obj);
});

app.listen(3000);
