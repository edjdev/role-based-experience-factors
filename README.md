# Role-based Experience Factors

## Running the prototype

1. Install Node 8.x https://nodejs.org/en/
1. Run `npm install` in the project directory
1. Run `node app.js` to start the local server
1. Visit `http://localhost:3000/`
  * Click on a role description
  * Click on the experience factor worksheet hyperlink to download it
  * Upload the TSV for Google Drive and open with Google Sheets
  * Change an value from `1` to `0.5` (or something) to make sure the formula is working
